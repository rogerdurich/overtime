import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { FilterQuery, Model, UpdateWriteOpResult } from 'mongoose';
import { IMessageResponse } from '../interfaces';

export interface ISearchOptions {
  start: number;
  limit: number;
  sort?: any;
}

export interface IPagination<T> {
  values: T[];
  morePages: boolean;
}

export class MongoBaseRepository<T> {
  constructor(private model: Model<any>) {}

  // Create a new entity in the database
  async create(entity: any): Promise<T> {
    // Set up a placeholder for the result
    let result: any;

    // Try to use the repository to create a new entity, store the result
    try {
      result = await this.model.create(entity);
    } catch (err) {
      throw new InternalServerErrorException(
        `❌ Unable to add a ${entity} because: ${err}`,
      );
    }

    // Return the result back to the user if all goes well.
    return result;
  }

  // Find all entities in the database
  async findAll(exclude?: string[]): Promise<T[]> {
    const result = await this.model.find().select(exclude);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  // Find a single entity in the database by ID
  async findById(id: string): Promise<T> {
    const result = await this.model.findById(id);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  // Find a single entity in the database by a filter param
  async findOne(filter: FilterQuery<T>): Promise<T> {
    const result = await this.model.findOne(filter);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  // Find and update a single entity in the database
  async update(id: string, params: any, exclude?: string): Promise<T> {
    // Set up the mongodb params
    const filter = { _id: new ObjectId(id) };
    const update = { $set: params };

    // Update the record atomically
    const result = await this.model
      .findOneAndUpdate(filter, update, {
        returnOriginal: false,
        useFindAndModify: false,
      })
      .select(exclude);

    // Treat the response if we don't receive back an entity
    if (!result) {
      throw new NotFoundException();
    }

    // Return the updated document
    return result;
  }

  async updateOne(filter: FilterQuery<T>, params: any): Promise<any> {
    // Set up the mongodb params
    const update = { $set: params };

    // Update the record atomically
    const result = await this.model.findOneAndUpdate(filter, update, {
      returnOriginal: false,
      useFindAndModify: false,
    });

    // Treat the response if we don't receive back an entity
    if (!result) {
      throw new NotFoundException();
    }

    // Return the updated document
    return result;
  }

  async updateMany(
    filter: FilterQuery<T>,
    params: any,
  ): Promise<UpdateWriteOpResult> {
    // Set up the mongodb params
    const update = { $set: params };

    // Update the records atomically
    const result = await this.model.updateMany(filter, update, {
      returnOriginal: false,
      useFindAndModify: false,
    });

    // Treat the response if we don't receive back an entity
    if (!result) {
      throw new NotFoundException();
    }

    // Return the updated document
    return result;
  }

  // Remove an entity from the database
  async remove(id: string): Promise<IMessageResponse> {
    // Find the item to delete
    const query = { _id: new ObjectId(id) };

    // Try to delete the item, throw an exception if an error is returned from the DB
    try {
      const result = await this.model.findOneAndDelete(query);
      if (!result) {
        return {
          message: `ID: ${id} was not found, so nothing was deleted.`,
        };
      }
    } catch (err) {
      throw new InternalServerErrorException();
    }

    // Return a message that matches the graphql schema
    return { message: `The deletion operation on ${id} was successful` };
  }
}
