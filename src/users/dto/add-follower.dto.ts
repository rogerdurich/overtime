import { PartialType } from '@nestjs/mapped-types';
import { IsEnum, IsString } from 'class-validator';
import { ActionsEnum } from '../../enums';
import { CreateUserDto } from './create-user.dto';

export class UpdateFollowersDto extends PartialType(CreateUserDto) {
  @IsString()
  @IsEnum(ActionsEnum)
  actions: string;

  @IsString()
  followerId: string;
}
