import { IsString, IsNumber, IsArray } from 'class-validator';
import { IUser } from '../../interfaces';

export class CreateUserDto implements IUser {
  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsNumber()
  views?: number;

  @IsArray()
  follows?: string[];
}
