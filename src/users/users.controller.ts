import {
  Body,
  Controller,
  Get,
  HttpException,
  Param,
  Post,
  Put,
  UseGuards,
  Response,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt.guard';
import { IUser } from 'src/interfaces';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { ActionsEnum } from 'src/enums';
import { UpdateFollowersDto } from './dto/add-follower.dto';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Get()
  async findAll(): Promise<IUser[]> {
    return await this.userService.findAll(['-password', '-follows']);
  }

  @Get(':id')
  async findById(@Param() params: any): Promise<IUser> {
    return await this.userService.findById(params.id);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateFollowersDto: UpdateFollowersDto,
  ): Promise<IUser> {
    switch (updateFollowersDto.actions) {
      case ActionsEnum.VIEW:
        return await this.userService.updateViews(id, '-password');
      case ActionsEnum.FOLLOW:
        return await this.userService.addFollowers(
          id,
          updateFollowersDto.followerId,
        );
      case ActionsEnum.UNFOLLOW:
        return await this.userService.removeFollowers(
          id,
          updateFollowersDto.followerId,
        );
      default:
        throw new HttpException(
          `Unrecognized action: ${updateFollowersDto.actions}`,
          400,
        );
    }
  }
}
