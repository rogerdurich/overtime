import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './user.schema';
import { MongoBaseRepository } from 'src/repositories/base.repository';
import { IUser } from 'src/interfaces';
import { Views } from 'src/utils/views';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) public userModel: Model<UserDocument>,
    private readonly views: Views,
  ) {
    this.userRepository = new MongoBaseRepository(userModel);
  }

  private userRepository: MongoBaseRepository<IUser>;

  async create(User: IUser): Promise<IUser> {
    const password = User.password;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);

    const user = this.userRepository.create({
      username: User.username,
      password: hash,
      follows: User.follows,
      views: User.views,
    });

    return {
      id: (await user).id,
      username: (await user).username,
      views: (await user).views,
      follows: (await user).follows,
    };
  }

  async findOne(username: string): Promise<IUser> {
    return await this.userRepository.findOne({ username: username });
  }

  async findById(id: string): Promise<IUser> {
    const user = await this.userRepository.findOne({ _id: id });
    return {
      id: id,
      username: user.username,
      follows: user.follows,
      views: user.views,
    };
  }

  async findAll(exclude: string[]): Promise<IUser[]> {
    return await this.userRepository.findAll(exclude);
  }

  async updateViews(id: string, exclude?: string): Promise<IUser> {
    const views = await this.views.incrementViews(id);
    return await this.userRepository.update(
      id,
      {
        views: views,
      },
      exclude,
    );
  }

  async addFollowers(
    id: string,
    followerId: string,
    exclude?: string,
  ): Promise<IUser> {
    const user = await this.userRepository.findById(id);
    if (user.follows.includes(followerId)) {
      throw new HttpException('This follower has already been assoicated', 400);
    }
    user.follows.push(followerId);
    return await this.userRepository.update(
      id,
      {
        follows: user.follows,
      },
      exclude,
    );
  }

  async removeFollowers(id: string, followerId: string): Promise<IUser> {
    const user = await this.userRepository.findById(id);
    const position = user.follows.indexOf(followerId);
    const follows = user.follows.slice(0, position);
    return await this.userRepository.update(id, {
      follows: follows,
    });
  }
}
