import { Injectable } from '@nestjs/common';
import { HealthCheck } from './interfaces';

@Injectable()
export class AppService {
  getHealth(): HealthCheck {
    const currentDate = new Date().toLocaleDateString('en-us');
    const currentTime = new Date().toLocaleTimeString('en-us');
    const timeStamp = `The current time is: ${currentTime} on: ${currentDate}`;
    return {
      status: 'OK',
      timeCheck: timeStamp,
    };
  }
}
