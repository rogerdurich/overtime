export interface HealthCheck {
  status: string;
  timeCheck: string;
}
