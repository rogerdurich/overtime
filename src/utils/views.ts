import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/interfaces';
import { MongoBaseRepository } from 'src/repositories/base.repository';
import { User, UserDocument } from 'src/users/user.schema';

@Injectable()
export class Views {
  constructor(@InjectModel(User.name) public userModel: Model<UserDocument>) {
    this.userRepository = new MongoBaseRepository(userModel);
  }

  private userRepository: MongoBaseRepository<IUser>;

  async getViews(id: string): Promise<number> {
    const user: IUser = await this.userRepository.findOne({ _id: id });
    let currentViews = user.views;
    if (!currentViews) {
      currentViews = 0;
    }
    return currentViews;
  }

  async incrementViews(id: string) {
    let views = await this.getViews(id);
    const updatedViews = ++views;
    return updatedViews;
  }

  async decrementViews(id: string) {
    let views = await this.getViews(id);
    const updatedViews = --views;
    return updatedViews;
  }
}
