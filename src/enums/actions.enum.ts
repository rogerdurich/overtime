export enum ActionsEnum {
  VIEW = 'view',
  FOLLOW = 'follow',
  UNFOLLOW = 'unfollow',
}
